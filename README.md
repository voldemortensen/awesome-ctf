# awesome-ctf
A list of awesome tools for CTFs.

Table of Contents
=================

- [Forensics](#forensics)
- [Networking](#networking)
- [Reverse Engineering](#reverse-engineering)

Keys
====
- :heavy_dollar_sign: - Product requires purchase or subscription (free trial may or may not be available)

## Forensics

- [Autopsy](https://www.sleuthkit.org/autopsy/) - a digital forensics platform and graphical interface to The Sleuth Kit® and other digital forensics tools
- [binwalk](https://github.com/ReFirmLabs/binwalk) - Firmware Analysis Tool
- [dd](http://man7.org/linux/man-pages/man1/dd.1.html) - convert and copy a file
- [foremost](https://linux.die.net/man/1/foremost) - Recover files using their headers, footers, and data structures
- [PhotoRec](https://www.cgsecurity.org/wiki/PhotoRec) - file data recovery software designed to recover lost files including video, documents and archives from hard disks, CD-ROMs, and lost pictures
- [strings](https://linux.die.net/man/1/strings) - print the strings of printable characters in files
- [TestDisk](https://www.cgsecurity.org/wiki/TestDisk) - powerful free data recovery software
- [volatility](https://github.com/volatilityfoundation/volatility) - an advanced memory forensics framework

## Networking

- netcat
- [ngrep](https://github.com/jpr5/ngrep) - grep for your network traffic
- [Nmap](https://nmap.org/) - Network scanning, discovery, and auditing
- telnet
- [WireEdit](https://wireedit.com/) - A WYSIWYG packet editor
- [WireShark](https://www.wireshark.org/) - the world’s foremost and widely-used network protocol analyzer

## Reverse Engineering

- [Binary Ninja](https://binary.ninja/) - A new kind of reversing platform :heavy_dollar_sign:
- [gdb](https://www.gnu.org/software/gdb/) - Step through binary execution
- [Hex Fiend](http://ridiculousfish.com/hexfiend/) - A fast and clever open source hex editor for Mac OS X
- [IDA](https://www.hex-rays.com/products/ida/index.shtml) - Cross-platform disassembler and debugger :heavy_dollar_sign:
- [objdump](https://linux.die.net/man/1/objdump) - Display information from object files
- [radare2](https://rada.re/n/) - Libre and Portable Reverse Engineering Framework
